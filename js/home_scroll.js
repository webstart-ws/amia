$(document).ready(function(){
  var block_show = null;
  function scrollTracking(){
    var wt = $(window).scrollTop();
    var wh = $(window).height();
    var fr = $('.one_brand').offset().top;
    var frs = $('.one_brand').outerHeight();
    var sc = $('.two_brand').offset().top;
    var sec = $('.two_brand').outerHeight();
    var dv = $('.free_brand').offset().top;
    var dev = $('.free_brand').outerHeight();
    var rf = $('.four_brand').offset().top;
    var ref = $('.four_brand').outerHeight();
    if (wt + wh >= fr && wt + wh - frs * 2 <= fr + (wh - frs)){
        $(".one_section_brand").addClass("brand_right_block_fix_section_active");
        $(".two_section_brand").removeClass("brand_right_block_fix_section_active");
    }else if (wt + wh >= sc && wt + wh - sec * 2 <= sc + (wh - sec)){
        $(".two_section_brand").addClass("brand_right_block_fix_section_active");
        $(".one_section_brand").removeClass("brand_right_block_fix_section_active");
        $(".free_section_brand").removeClass("brand_right_block_fix_section_active");
    } else  if (wt + wh >= dv && wt + wh - dev * 2 <= dv + (wh - dev)){
        $(".free_section_brand").addClass("brand_right_block_fix_section_active");
        $(".two_section_brand").removeClass("brand_right_block_fix_section_active");
        $(".four_section_brand").removeClass("brand_right_block_fix_section_active");
    } else if (wt + wh >= rf && wt + wh - ref * 2 <= rf + (wh - ref)){
        $(".four_section_brand").addClass("brand_right_block_fix_section_active");
        $(".free_section_brand").removeClass("brand_right_block_fix_section_active");
    } else {
      if (block_show == null || block_show == true) {
        $(".one_section_brand").removeClass("brand_right_block_fix_section_active");
        $(".two_section_brand").removeClass("brand_right_block_fix_section_active");
        $(".free_section_brand").removeClass("brand_right_block_fix_section_active");
        $(".four_section_brand").removeClass("brand_right_block_fix_section_active");
      }
      block_show = false;
    }
  }
  $(window).scroll(function(){
    scrollTracking();
  });
  $(document).ready(function(){ 
    scrollTracking();
  });

});
